<?php
include('db.php');

$count = "";
$mysql = "select * from resume";
$res = mysqli_query($conn, $mysql);
$count = mysqli_num_rows($res);
header('Content-Type:application/json');
if ($count > 0) {
    while ($row = mysqli_fetch_assoc($res)) {
        $arr[] = $row;
    }
    echo json_encode(['status' => 'true', 'data' => $arr, 'result' => 'found']);
} else {
    echo json_encode(['status' => 'true', 'data' => 'no data found', 'result' => ' not found']);
}
